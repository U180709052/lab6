class Circle{
    Point center;
    int radius;

    public Circle(Point c, int r){
        this.center = c;
        this.radius = r;
    }

    public double area(){
        return Math.PI*Math.pow(this.radius, 2);
    }

    public double perimeter(){
        return Math.PI*this.radius*2;
    }
    public boolean intersect(Circle otherCircle){
        return  (radius+otherCircle.radius) >= Math.sqrt(Math.pow(center.xCoord - otherCircle.center.xCoord, 2) + 
                    Math.pow(center.yCoord - otherCircle.center.yCoord, 2)) ? true : false;
    }
}
